const URLParameters = new URLSearchParams(window.location.search);

const UUID_primary = "46039343-5555-4763-bbe6-27f4090900fa";
const UUID_tx = "f746fbd8-00a1-4b73-a78e-c6930248d9d2";
const UUID_fpars = "f846fbd8-00a1-4b73-a78e-c6930248d9d2";
const UUID_measurement = "fa46fbd8-00a1-4b73-a78e-c6930248d9d2";

const status_element = document.getElementById('status');
var device;
var char_tx;
var char_fpars;

window.onload = function() {
  wereConnected(false);
};

async function select() {
    try {
        console.log('Request a device');
        device = await navigator.bluetooth.requestDevice({
            filters: [
                {services: [UUID_primary]},
                {namePrefix: document.querySelector('#deviceNamePrefix').value}
            ]
        });
        device.addEventListener('gattserverdisconnected', onDisconnected);
        console.log('Got device: ' + device.name + ' (' + device.id + ')');
        document.getElementById('connect.select').disabled = true;
        connect();
    } catch (error) {
        console.log('Error: ' + error);
    }
}

async function connect() {
    try {

        status_line('Connecting...');
        const server = await device.gatt.connect();

        console.log('Get the primary service');
        const service = await server.getPrimaryService(UUID_primary);

        console.log('getCharacteristics');
        char_tx = await service.getCharacteristic(UUID_tx);
        char_fpars = await service.getCharacteristic(UUID_fpars);

        char_measurement = await service.getCharacteristic(UUID_measurement);
        char_measurement.addEventListener('characteristicvaluechanged', handleMeasurement);
        await char_measurement.startNotifications();

        status_line('Connected to ' + server.device.name);
        wereConnected(true)

        // URL parameter hw_version can override the device name
        let hw_version = URLParameters.get('hw_version');
        updateMeasurements(hw_version ? hw_version : server.device.name);

    } catch (error) {
        status_line('Error:  ' + error);
        await device.gatt.disconnect();
    }
}

function status_line(str) {
    status_element.value += str + "\n";
    status_element.scrollTop = status_element.scrollHeight;
}

function onDisconnected() {
    status_line("Disconnected");
    wereConnected(false)
}

function wereConnected(state) {
    document.getElementById('profisafe.provision').disabled = !state
    document.getElementById('isa100.provision').disabled = !state;
    document.getElementById('connect.reconnect').disabled = (device==null) || state;
    document.getElementById('connect.disconnect').disabled = !state;
}

async function disconnect() {
    if (!device?.gatt?.connected) {
        return;
    }
    console.log("Disconnecting");
    await device.gatt.disconnect();
}

var measurements;

function updateMeasurements(hw_version) {
    const measurementNames = (() => {
        switch (hw_version) {
            case 'Mimes RHUB IO-1.1.0':
                return [
                    'PSU', 'MCU', 'WISA', '4_20', 'Relay', 'BIST', '', '', ''
                ];
            case 'Mimes RHUB Serial-1.1.0':
                return [
                    'PSU', 'LDO', '', '', '', 'VCHECK', 'BAT', '', ''
                ];
            default:
                return ['1', '2', '3', '4', '5', '6', '7', '8', '9'];
        }
    })();

    document.getElementById('measurement-container').innerHTML = '';
    // Create elements for each index
    measurements = measurementNames.map((name, index) => {
        // Skip empty indexNames
        if (!name) return null;

        const element = document.createElement('div');
        element.className = 'measurement';
        element.innerHTML = `<span class="connection-label">${name}</span> <span class="measurement-value">5.000</span>`;
        document.getElementById('measurement-container').appendChild(element);
        return element;
    });
}

function buf2hex(buffer) { // buffer is an ArrayBuffer
    return [...new Uint8Array(buffer)]
        .map(x => x.toString(16).padStart(2, '0'))
        .join('');
}

function handleMeasurement(event) {
    let measurement = event.target.value;
    const index = measurement.getUint8(0);
    const value = measurement.getFloat32(1, true)
    console.debug(`> Measurement ${index} is ${value} (${buf2hex(measurement.buffer)})`);

    const element = measurements[index];
    if (element) {
        const span = element.querySelector('.measurement-value');
        span.textContent = Number(value).toFixed(3);
    }
}


async function profisafe_provision(host_address, device_address, watchdog) {
    status_line(`Sending fparam host address ${host_address}, device address ${device_address} and watchdog ${watchdog}`)

    try {
        let bytes = new ArrayBuffer(6);
        let view = new DataView(bytes);
        view.setUint16(0, host_address, false);
        view.setUint16(2, device_address, false);
        view.setUint16(4, watchdog, false);
    } catch (error) {
        status_line('Error:  ' + error);
        await device.gatt.disconnect();
    }
}

async function isa100_prefix_and_send(prefix, value) {
    let payload = [prefix].concat([...new TextEncoder().encode(value)]);

    await char_tx.writeValue(new Uint8Array([0].concat(payload.slice(0, 19))));
    if (payload.length > 19) {
        await char_tx.writeValue(new Uint8Array([1].concat(payload.slice(19))));
    }
    await new Promise(r => setTimeout(r, 500));
}

async function isa100_provision(elements) {
    for (let e of elements) {
        if (e.value.length < e.minLength || e.value.length > e.maxLength) {
            status_line(`${e.id} is too short or long`);
            return;
        }
    }
    status_line(`Provisioning started`);

    await isa100_prefix_and_send(0, []); // Start provisioning
    await isa100_prefix_and_send(1, elements['isa100.name'].value);
    await isa100_prefix_and_send(2, elements['isa100.MAC'].value)
    await isa100_prefix_and_send(3, elements['isa100.joinkey'].value);
    await isa100_prefix_and_send(4, elements['isa100.utc_offset'].value);
    await isa100_prefix_and_send(5, elements['isa100.role'].value);
    await isa100_prefix_and_send(6, elements['isa100.bitmask'].value);
    await isa100_prefix_and_send(7, elements['isa100.id'].value);
    await isa100_prefix_and_send(8, []); // Activate

    status_line(`Provisioning done`);
}
