// Based on code under MIT license:
// Copyright (c) 2022 Nick Poole
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


let portOpen = false; // tracks whether a port is corrently open
let portPromise; // promise used to wait until port succesfully closed
let port; // current SerialPort object
let reader; // current port reader object so we can call .cancel() on it to interrupt port reading
let profisafeParameterPresent = false; // Has user specified any of these in the URL?

const CHANNEL_PROFISAFE = 1;
const CHANNEL_ISA100 = 2;


// Do these things when the window is done loading
window.onload = function () {
    // Check to make sure we can actually do serial stuff
    if ("serial" in navigator) {
        // The Web Serial API is supported.
        // Connect event listeners to DOM elements
        document
            .getElementById("openclose_port")
            .addEventListener("click", openClose);
        document
            .getElementById("term_window")
            .addEventListener("keypress", onKeypress);

        // Clear the term_window textarea
        clearTerminal();

        // See if there's a prefill query string on the URL
        const params = new Proxy(new URLSearchParams(window.location.search), {
            get: (searchParams, prop) => searchParams.get(prop),
        });

        const keysToSet = ["host_address", "device_address", "watchdog"];

        // Loop through the profisafe keys and set values if they exist in the params
        keysToSet.forEach(key => {
            const value = params[key];
            if (value !== null && value !== undefined) {
                const element = document.getElementById(`profisafe.${key}`);
                if (element) {
                    element.value = value;
                    profisafeParameterPresent = true;
                }
            }
        });

    } else {
        // The Web Serial API is not supported.
        // Warn the user that their browser won't do stupid serial tricks
        alert("The Web Serial API is not supported by your browser");
    }
};

function parseOutput(inputString) {
    const patterns = [
        {pattern: /PS host addr:\s+(\d+)/, variable: 'profisafe.host_address'},
        {pattern: /PS device addr:\s+(\d+)/, variable: 'profisafe.device_address'},
        {pattern: /PS watchdog:\s+(\d+)/, variable: 'profisafe.watchdog'},
        {pattern: /PS FPar CRC:\s+(\d+)/, variable: 'profisafe.fpar_crc'}
    ];

    patterns.forEach(({pattern, variable}) => {
        const match = inputString.match(pattern);
        if (match) {
            // If a match is found, assign the value to the corresponding variable
            document.getElementById(variable).value = parseInt(match[1]);
        }
    });
}

// This function is bound to the "Open" button, which also becomes the "Close" button
// and it detects which thing to do by checking the portOpen variable
async function openClose() {
    // Is there a port open already?
    if (portOpen) {
        // Port's open. Call reader.cancel() forces reader.read() to return done=true
        // so that the read loop will break and close the port
        reader.cancel();
        console.log("attempt to close");
    } else {
        // No port is open so we should open one.
        // We write a promise to the global portPromise var that resolves when the port is closed
        portPromise = new Promise((resolve) => {
            // Async anonymous function to open the port
            (async () => {
                // If we haven't stashed a SerialPort then ask the user to select one
                port = await navigator.serial.requestPort();

                await port.open({baudRate: 115200});

                // Create a textDecoder stream and get its reader, pipe the port reader to it
                const textDecoder = new TextDecoderStream();
                reader = textDecoder.readable.getReader();
                const readableStreamClosed = port.readable.pipeTo(textDecoder.writable);

                // If we've reached this point then we're connected to a serial port
                // Set a bunch of variables and enable the appropriate DOM elements
                portOpen = true;
                document.getElementById("openclose_port").innerText = "Close";
                document.getElementById('profisafe.provision').disabled = false;
                document.getElementById('isa100.provision').disabled = false;

                if (!profisafeParameterPresent) {
                    // Request profisafe provision parameters so we can populate the form with the current values
                    await serial_write(new Uint8Array([CHANNEL_PROFISAFE, 0]));
                }

                // Serial read loop. We'll stay here until the serial connection is ended externally or reader.cancel() is called
                // It's OK to sit in a while(true) loop because this is an async function and it will not block while it's await-ing
                // When reader.cancel() is called by another function, reader will be forced to return done=true and break the loop
                let lineBuffer = '';

                while (true) {
                    const {value, done} = await reader.read();
                    if (done) {
                        reader.releaseLock(); // release the lock on the reader so the owner port can be closed
                        break;
                    } else {
                        lineBuffer += value;
                        const lines = lineBuffer.split('\n');

                        // Process complete lines
                        for (const line of lines.slice(0, -1)) {
                            parseOutput(line);
                        }

                        // Update the lineBuffer with the remaining incomplete line
                        lineBuffer = lines[lines.length - 1];
                    }
                    document.getElementById("term_window").value += value; // write the incoming string to the term_window textarea
                    document.getElementById("term_window").scrollTop = document.getElementById("term_window").scrollHeight;
                }

                // If we've reached this point then we're closing the port
                // first step to closing the port was releasing the lock on the reader
                // we did this before exiting the read loop.
                // That should have broken the textDecoder pipe and propagated an error up the chain
                // which we catch when this promise resolves
                await readableStreamClosed.catch(() => {
                    /* Ignore the error */
                });
                // Now that all of the locks are released and the decoder is shut down, we can close the port
                await port.close();

                // Set a bunch of variables and disable the appropriate DOM elements
                portOpen = false;
                document.getElementById("openclose_port").innerText = "Open";
                document.getElementById('profisafe.provision').disabled = true;
                document.getElementById('isa100.provision').disabled = true;

                console.log("port closed");

                // Resolve the promise that we returned earlier. This helps other functions know the port status
                resolve();
            })();
        });
    }
}

// Send a string over the serial port.
// This is easier than listening because we know when we're done sending
async function sendString(outString) {
    // Get a text encoder, pipe it to the SerialPort object, and get a writer
    const textEncoder = new TextEncoderStream();
    const writableStreamClosed = textEncoder.readable.pipeTo(port.writable);
    const writer = textEncoder.writable.getWriter();

    // write the outString to the writer
    await writer.write(outString);

    // close the writer since we're done sending for now
    await writer.close();
    await writableStreamClosed;
}

// Clear the contents of the term_window textarea
function clearTerminal() {
    document.getElementById("term_window").value = "";
}

// This function in bound to "keydown" in the term_window textarea.
// It intercepts keystrokes and calls the sendString function
function onKeypress(e) {
    const key = e.keyCode;

    // If the user has pressed enter
    if (key === 13) {
        e.preventDefault();
    }
    serial_write(new Uint8Array([key]));
}

async function serial_write(bytes) {
    if (port.writable.locked) { // Is user typing too fast
        return;
    }

    const writer = port.writable.getWriter();

    try {
        for (let c of bytes) {
            await writer.write(new Uint8Array([c]));
            // We have no HW flowcontrol so we transmit slowly
            // Now that we have interrupt driven RX this could probably be much lower
            await new Promise(r => setTimeout(r, 100));
        }
    } catch (error) {
        console.error(error);
    }

    writer.releaseLock();
}

async function profisafe_provision(host_address, device_address, watchdog) {
    console.log(`Sending fparam host address ${host_address}, device address ${device_address} and watchdog ${watchdog}`)

    // Clear the previous fpar_crc awaiting an updated value
    document.getElementById('profisafe.fpar_crc').value = '';

    let buffer = new ArrayBuffer(8);
    let view = new DataView(buffer);
    view.setUint8(0, CHANNEL_PROFISAFE);
    view.setUint8(1, 6);
    view.setUint16(2, host_address, false);
    view.setUint16(4, device_address, false);
    view.setUint16(6, watchdog, false);
    const bytes = new Uint8Array(buffer);

    await serial_write(bytes);
    await new Promise(r => setTimeout(r, 3000));    // Wait while the device reboots
    await serial_write(new Uint8Array([CHANNEL_PROFISAFE, 0])); // Load the new values
}


async function isa100_prefix_and_send(prefix, value) {
    let payload = [CHANNEL_ISA100, value.length+1, prefix].concat([...new TextEncoder().encode(value)]);

    await serial_write(payload);
}

async function isa100_provision(elements) {
    for (let e of elements) {
        if (e.value.length < e.minLength || e.value.length > e.maxLength) {
            console.error(`${e.id} is too short or long`);
            return;
        }
    }

    await isa100_prefix_and_send(0, []); // Start provisioning
    await isa100_prefix_and_send(1, elements['isa100.name'].value);
    await isa100_prefix_and_send(2, elements['isa100.MAC'].value)
    await isa100_prefix_and_send(3, elements['isa100.joinkey'].value);
    await isa100_prefix_and_send(4, elements['isa100.utc_offset'].value);
    await isa100_prefix_and_send(5, elements['isa100.role'].value);
    await isa100_prefix_and_send(6, elements['isa100.bitmask'].value);
    await isa100_prefix_and_send(7, elements['isa100.id'].value);
    await isa100_prefix_and_send(8, []); // Activate
}

